.DEFAULT_GOAL = run

CFLAGS =-pedantic -Wall -Werror -Wconversion -Wshadow -Wpointer-arith \
	-Wcast-qual -Wcast-align -Wstrict-prototypes \
	$(shell pkg-config --cflags x11)

LFLAGS = $(shell pkg-config --libs x11)

out:
	mkdir -p out 2>&1 > /dev/null
 
out/%.o: %.c out
	gcc $(CFLAGS) -c -o $@ $<

ubar: out/ubar.o
	gcc $(LFLAGS) -o $@ $<

.PHONY: run
run: ubar
	./ubar

.PHONY: run
valgrind: ubar
	gcc $(CFLAGS) -ggdb $(LFLAGS) -o ubar ubar.c
	valgrind --leak-check=full --track-origins=yes ./ubar
