/* BEGIN ubar.c */
/*
 * ubar.c - A simple status bar for X11 which is unicode/multi-language capable.
 * Adapted from http://rio.la.coocan.jp/lab/xlib/011mbcs.htm
 */
#include <X11/Xlib.h>
#include <X11/Xlocale.h> /* ロケール用ヘッダーファイル */
#include <stdio.h>
#include <string.h>

/* Configurations for bar. */
static char     *cfg_font   = "-*-fixed-medium-r-normal--16-*-*-*";
static unsigned  cfg_width  = 120;
static unsigned  cfg_height = 20;
static int       cfg_x      = 200;
static int       cfg_y      = 200;
static unsigned  cfg_border = 2;

/* Main */
int main( void )
{
    Display*      dpy;            /* ディスプレイ変数 */
    Window        root;           /* ルートウィンドウ */
    Window        win;            /* 表示するウィンドウ */
    int           screen;         /* スクリーン */
    unsigned long black,white;    /* 黒と白のピクセル値 */
    GC            gc;             /* グラフィックスコンテキスト */
    XEvent        evt;            /* イベント構造体 */

    XFontSet      fs;             /* フォントセット */
    int           missing_count;  /* 存在しない文字集合の数 */
    char**        missing_list;   /* 存在しない文字集合 */
    char*         def_string;     /* ↑に対して描画される文字列 */


    /* 描画する全角文字列 */
    char*         string = "こんにちは、お元気ですか？.";


    /* ロケールを設定する(現在システムに設定されているロケールを使用) */
    if ( setlocale( LC_CTYPE, "" ) == NULL ) {
        printf( "Can't set locale\n" );
        return 1;
    }

    /* Xlib が現在のロケールを扱えるかどうかを判断する */
    if ( ! XSupportsLocale() ) {
        printf( "Current locale is not supported\n" );
        return 1;
    }


    dpy = XOpenDisplay( "" );

    root   = DefaultRootWindow( dpy );
    screen = DefaultScreen( dpy );
    white  = WhitePixel( dpy, screen );
    black  = BlackPixel( dpy, screen );


    win = XCreateSimpleWindow( dpy, root,
            cfg_x, cfg_y, cfg_width, cfg_height, cfg_border, black, white );


    gc = XCreateGC( dpy, win, 0, NULL );
    XSetBackground( dpy, gc, white );
    XSetForeground( dpy, gc, black );


    /* フォントセットを生成する */
    fs = XCreateFontSet( dpy, cfg_font,
            &missing_list, &missing_count, &def_string );

    if ( fs == NULL ) {
        printf( "Failed to create fontset\n" );
        return 1;
    }

    XFreeStringList( missing_list );

    XSelectInput( dpy, win, ExposureMask | KeyPressMask );
    XMapWindow( dpy, win );


    while( 1 ) {
        XNextEvent( dpy, &evt );

        switch( evt.type ) {
            case Expose:
                if( evt.xexpose.count == 0 ) {
                    XmbDrawString( dpy, win, fs, gc, 
                            0, 20, string, (int) strlen(string) );
                }
                break;

            case KeyPress:
                XFreeGC( dpy, gc );
                XFreeFontSet( dpy, fs );
                XDestroyWindow( dpy, win );
                XCloseDisplay( dpy );
                return 0;
        }
    }
}

/* END ubar.c */
